extends CharacterBody2D

class_name GMashl
var BombClass = preload("res://bomb.tscn")
var SporeScene = preload("res://spore.tscn")
@export var speed = 60.0
@export var initial_interval = 4
var interval: float
var splashed := false
var routine := 0

func _ready():
	$AnimationPlayer.play("MashlWalk")
	
func _physics_process(_delta):
	interval -= _delta
	if interval < 0:
		next_routine()
		interval = initial_interval
	
	move_and_slide()
	var collider := get_last_slide_collision()
	if collider:
		var collider_obj = collider.get_collider()
		if collider_obj is Player:
			(collider_obj as Player).miss()
		if collider_obj is Bullet:
			splash()

func release_spore():
	var t2d = Transform2D()
	for i in 4:
		var spore = SporeScene.instantiate()
		get_parent().add_child(spore)
		spore.velocity = t2d.rotated(deg_to_rad(90 * i)) * Vector2(32, 0)
		spore.position = position

func splash():
	if not splashed:
		splashed = true
		modulate = Color.RED
		var t2d = Transform2D()
		for i in 8:
			var bomb = BombClass.instantiate()
			get_parent().add_child(bomb)
			bomb.position = position + t2d.rotated(deg_to_rad(135 * i)) * Vector2(32, 0)
			await get_tree().create_timer(0.1).timeout
	queue_free()
	
func next_routine():
	routine = randi() % 5
	match routine:
		4:
			velocity = Vector2.ZERO
			release_spore()
		3:
			velocity = Vector2.LEFT * speed
		2:
			velocity = Vector2.DOWN * speed
		1:
			velocity = Vector2.RIGHT * speed
		0:
			velocity = Vector2.UP * speed



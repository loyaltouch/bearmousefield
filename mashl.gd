extends CharacterBody2D

class_name Mashl
var BombClass = preload("res://bomb.tscn")
@export var speed = 80.0
var start_position: Vector2
var splashed = false

func _ready():
	start_position = position
	$AnimationPlayer.play("Mashl_walk")
	
func _physics_process(_delta):
	velocity = near()
	move_and_slide()
	var collider := get_last_slide_collision()
	if collider:
		var collider_obj = collider.get_collider()
		if collider_obj is Player:
			(collider_obj as Player).miss()
		if collider_obj is Bullet:
			splash()

func splash():
	if not splashed:
		splashed = true
		modulate = Color.RED
		var t2d = Transform2D()
		for i in 8:
			var bomb = BombClass.instantiate()
			get_parent().add_child(bomb)
			bomb.position = position + t2d.rotated(deg_to_rad(135 * i)) * Vector2(32, 0)
			await get_tree().create_timer(0.1).timeout
	queue_free()

func reset():
	position = start_position
	visible = true
	collision_layer = 1
	modulate = Color.WHITE
	

func near() -> Vector2:
	if not splashed:
		return (Global.player.global_position - global_position).normalized() * speed
	else:
		return Vector2.ZERO

func reseet():
	for obj in get_children():
		if obj is Mashl:
			obj.queue_free()


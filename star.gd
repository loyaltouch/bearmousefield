extends CharacterBody2D

func _ready():
	%AnimationPlayer.play("StarAime")
	await get_tree().create_timer(0.5).timeout
	queue_free()

func _physics_process(_delta):
	move_and_slide()
	var collider := get_last_slide_collision()
	if collider:
		var collider_obj = collider.get_collider()
		if collider_obj is TileMap:
			queue_free()
		if collider_obj is Player:
			var player = collider_obj as Player
			player.miss()

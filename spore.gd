extends Sprite2D

var velocity: Vector2
var speed := 2
var GMashlScene = preload("res://g_mashl.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var tween := create_tween()
	#tween.set_parallel()
	tween.tween_property(self, "modulate", Color(1, 1, 1, 0), 1)
	#tween.tween_property(self, "position", position + velocity, 1)
	tween.play()
	await tween.finished
	spore_finish()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	position += velocity * speed * _delta

func spore_finish():
	var g_msl = GMashlScene.instantiate()
	get_parent().add_child(g_msl)
	g_msl.position = position
	queue_free()

extends Area2D

class_name Bomb

# Called when the node enters the scene tree for the first time.
func _ready():
	rotate(deg_to_rad(randi() % 360))
	await get_tree().create_timer(0.5).timeout
	queue_free()

func _on_body_entered(body):
	if body is Player:
		(body as Player).miss()

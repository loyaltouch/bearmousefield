extends StaticBody2D

class_name Barrel

var Star = preload("res://star.tscn")
var BombClass = preload("res://bomb.tscn")

func _physics_process(_delta: float) -> void:
	pass

func splash():
	if visible:
		modulate = Color.RED
		var t2d = Transform2D()
		for i in 8:
			var bomb = BombClass.instantiate()
			get_parent().add_child(bomb)
			bomb.position = position + t2d.rotated(deg_to_rad(135 * i)) * Vector2(32, 0)
			await get_tree().create_timer(0.1).timeout
	queue_free()

extends CharacterBody2D

class_name Player

var BulletClass = preload("res://bullet.tscn")

@export var speed:int = 200
var direction:Vector2
var direction_bullet: = Vector2(1, 0)
var flip_h: bool:
	set(value):
		%Sprite2D.flip_h = value

func _ready():
	Global.player = self
	%AnimationPlayer.current_animation = "Player_walk"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(_delta):
	if Input.is_action_just_pressed("ui_left"):
		flip_h = true
	if Input.is_action_just_pressed("ui_right"):
		flip_h = false
	direction = Input.get_vector("ui_left", "ui_right", "ui_up", "ui_down")
	velocity = speed * direction
	if not Global.main.missed or not Global.main.cleared:
		walk()

func _input(event):
	if event.is_action_pressed("ui_accept"):
		shot()

func walk():
	if direction.length() > 0:
		%AnimationPlayer.play("Player_walk")
		direction_bullet = direction
	else:
		%AnimationPlayer.stop()
	move_and_slide()

func shot():
	var bullet = BulletClass.instantiate()
	bullet.position = position + direction_bullet * 32
	bullet.direction = direction_bullet
	Global.main.add_child(bullet)

func miss():
	%AnimationPlayer.current_animation = "Player_damage"	
	Global.main.miss()

func reset():
	%AnimationPlayer.current_animation = "Player_walk"

extends Node2D

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _draw():
	var points = []
	points.append(Vector2(0, 0))
	points.append(Vector2(32, 16))
	points.append(Vector2(0, 32))
	var colors = []
	colors.append(Color.WHITE)
	colors.append(Color.WHITE)
	colors.append(Color.WHITE)
	draw_polygon(points, colors)
	draw_polyline(points, Color.BLACK, 3)

extends Node2D

class_name StageSelect

var stage_names = []
var labels = []
var selindex = 0
var cursol_position_diff = Vector2(440, 282)

func _init():
	pass

# Called when the node enters the scene tree for the first time.
func _ready():
	for i in 5:
		var new_label = %Label.duplicate()
		var stage_name = "STAGE%d" % [i + 1]
		new_label.text = stage_name
		%StageList.add_child(new_label)
		labels.append(new_label)
		stage_names.append("stage_%d" % i)
	$Cursol.position = Vector2.ZERO - cursol_position_diff
	stage_names[0] = "stage_1"
	stage_names[1] = "stage_2"
	stage_names[2] = "stage_3"
	stage_names[3] = "stage_4"
	stage_names[4] = "stage_5"

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass
	
func _input(event):
	if(event.is_action_pressed("ui_down")):
		selindex += labels.size() + 1
		selindex %= labels.size()
		cursol_move(selindex)
	if(event.is_action_pressed("ui_up")):
		selindex += labels.size() - 1
		selindex %= labels.size()
		cursol_move(selindex)

func cursol_move(index: int):
	if index < 0:
		index = 0
	if index > labels.size():
		index = labels.size() - 1
	var tween = get_tree().create_tween()
	tween.tween_property($Cursol, "position", labels[index].position - cursol_position_diff, 0.2)
#	%Cursol.position = labels[index].position - cursol_position_diff

func get_stage_name() -> String:
	if 0 <= selindex && selindex < stage_names.size():
		return stage_names[selindex]
	return "select"

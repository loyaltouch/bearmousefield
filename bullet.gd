extends CharacterBody2D
class_name Bullet

@export var speed = 300.0
var direction:Vector2

# Called when the node enters the scene tree for the first time.
func _ready():
	%AnimationPlayer.play("change_color")

func _physics_process(_delta: float) -> void:
	velocity = speed * direction # px/s
	move_and_slide()
	var collider := get_last_slide_collision()
	if collider:
		#print("Collider: %s"%collider.get_collider().name)
		if collider.get_collider() is Barrel:
			var barrel: Barrel
			barrel = collider.get_collider()
			barrel.splash()
		if collider.get_collider() is Mashl:
			(collider.get_collider() as Mashl).splash()
		if collider.get_collider() is GMashl:
			(collider.get_collider() as GMashl).splash()
		queue_free()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _draw():
	draw_circle(Vector2(0, 0), 4, Color.WHITE)

func _on_visible_on_screen_notifier_2d_screen_exited():
#	print("offscreen")
	queue_free()

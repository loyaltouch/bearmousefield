extends Node2D

class_name Main

var missed = false
var cleared = false
var stage_name = "select"
var StageObj

# Called when the node enters the scene tree for the first time.
func _ready():
	Global.main = self
	setup()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass

func _physics_process(_delta):
	pass

func _input(event):
	if event.is_action_pressed("ui_accept"):
		if missed:
			stage_name = "select"
			setup()
		if cleared:
			stage_name = "select"
			setup()
		if StageObj is StageSelect:
			stage_name = (StageObj as StageSelect).get_stage_name()
			setup()
	if event.is_action_pressed("ui_cancel"):
		if missed:
			stage_name = "select"
			setup()
		if cleared:
			stage_name = "select"
			setup()
	

func setup():
	$Miss.visible = false
	missed = false
	$Clear.visible = false
	cleared = false
	for stage in $stage.get_children():
		stage.queue_free()
	StageObj = load("res://%s.tscn" % [stage_name]).instantiate()
	$stage.add_child(StageObj)

func miss():
	$Miss.visible = true
	missed = true

func clear():
	$Clear.visible = true
	cleared = true
